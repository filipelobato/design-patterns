package com.designpatterns.gof.creational.factorymethod;

public class FactoryMethod {	     
	public static void main(String args[]) {
        FactoryPessoa factory1 = new FactoryPessoa();
        String nome1 = "Carlos";
        String sexo1 = "F";
        factory1.getPessoa(nome1, sexo1);
        
        FactoryPessoa factory2 = new FactoryPessoa();
        String nome2 = "Carlos";
        String sexo2 = "M";
        factory2.getPessoa(nome2, sexo2);
    }
}
