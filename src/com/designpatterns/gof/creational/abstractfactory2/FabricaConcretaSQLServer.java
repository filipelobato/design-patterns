package com.designpatterns.gof.creational.abstractfactory2;

public class FabricaConcretaSQLServer extends FabricaAbstrata {
	public ConectorAbstrato criarConector() {
		return new ConectorSQLServer();
	}

}
