package com.designpatterns.gof.creational.abstractfactory2;

public class ConectorSQLServer implements ConectorAbstrato {
	public void conectar() {
		// Realiza a conexão com o banco de dados SQLServer
	}
}
