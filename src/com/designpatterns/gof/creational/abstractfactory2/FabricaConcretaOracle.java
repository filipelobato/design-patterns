package com.designpatterns.gof.creational.abstractfactory2;

public class FabricaConcretaOracle extends FabricaAbstrata {
	public ConectorAbstrato criarConector() {
		return new ConectorOracle();
	}
}
