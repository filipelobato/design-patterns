package com.designpatterns.gof.creational.abstractfactory2;

public class Configuracao {

	public static final int ORACLE = 1;
	public static final int SQL_SERVER = 2;
	
	private String bancoDeDados;

	public String getBancoDeDados() {
		return bancoDeDados;
	}

	public void setBancoDeDados(String bancoDeDados) {
		this.bancoDeDados = bancoDeDados;
	}
	
}

