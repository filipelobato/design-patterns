package com.designpatterns.gof.creational.abstractfactory2;

public class ConectorOracle implements ConectorAbstrato {

	public void conectar() {
		// Realiza a conexão com o banco de dados Oracle
	}

}