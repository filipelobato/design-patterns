package com.designpatterns.gof.creational.abstractfactory2;

public abstract class FabricaAbstrata {

	  public abstract ConectorAbstrato criarConector();

	  public static FabricaAbstrata criarFabricaConcreta() throws Exception {
	    Configuracao configuracao = new Configuracao();
	    if (configuracao.getBancoDeDados().equals(Configuracao.ORACLE)) {
	    	return new FabricaConcretaOracle();
	    } else if (configuracao.getBancoDeDados().equals(Configuracao.SQL_SERVER)) {
	    	return new FabricaConcretaSQLServer();
	    } else {
	    	throw new Exception("Configuração de banco de dados não definida");
	    }
	  }

	}
