package com.designpatterns.gof.creational.abstractfactory2;

public interface ConectorAbstrato {
	public void conectar();
}
