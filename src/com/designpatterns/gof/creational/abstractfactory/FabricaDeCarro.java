package com.designpatterns.gof.creational.abstractfactory;

public class FabricaDeCarro {
    CarroSedan criarCarroSedan();
    CarroPopular criarCarroPopular();
}
