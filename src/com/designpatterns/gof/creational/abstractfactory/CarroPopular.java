package com.designpatterns.gof.creational.abstractfactory;

public interface CarroPopular {
    void exibirInfoSedan();
}
