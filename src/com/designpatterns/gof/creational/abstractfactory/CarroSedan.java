package com.designpatterns.gof.creational.abstractfactory;

public interface CarroSedan {
    void exibirInfoPopular();
}
