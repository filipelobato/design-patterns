package com.designpatterns.gof.creational.abstractfactory;

public class Siena implements CarroSedan {

	@Override
	public void exibirInfoPopular() {
		System.out.println("Modelo: Palio\nFábrica: Fiat\nCategoria:Popular");
	}

}
