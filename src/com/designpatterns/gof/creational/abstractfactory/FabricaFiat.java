package com.designpatterns.gof.creational.abstractfactory;

public class FabricaFiat extends FabricaDeCarro {
    @Override
    public CarroSedan criarCarroSedan() {
        return new Siena();
    }
 
    @Override
    public CarroPopular criarCarroPopular() {
        return new Palio();
    }
 
}
