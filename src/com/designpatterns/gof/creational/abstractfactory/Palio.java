package com.designpatterns.gof.creational.abstractfactory;

public class Palio implements CarroPopular {

	@Override
	public void exibirInfoSedan() {
		System.out.println("Modelo: Palio\nFábrica: Fiat\nCategoria:Popular");
	}

}
