package com.designpatterns.gof.structural.decorator;

/**
 * Concrete component
 * @author 05155211328
 *
 */
public class Cachaca extends Coquetel {
    public Cachaca() {
        nome = "Cachaça";
        preco = 1.5;
    }
}