package com.designpatterns.gof.structural.decorator;

/**
 * Component
 */
public abstract class Coquetel {
	String nome;
	double preco;
	
	public String getNome() {
		return nome;
	}
	
	public double getPreco() {
		return preco;
	}
}

