package com.designpatterns.gof.structural.decorator;

/**
 * anexa responsabilidades adicionais a um objeto dinamicamente.
 * Fornece uma alternativa flexível em relação à herança para estender
 * funcionalidades.
 */
public class Decorator {
	public static void main(String[] args) {
		Coquetel meuCoquetel = new Cachaca();
        System.out.println(meuCoquetel.getNome() + " = "
                + meuCoquetel.getPreco());
 
        meuCoquetel = new Refrigerante(meuCoquetel);
        System.out.println(meuCoquetel.getNome() + " = "
                + meuCoquetel.getPreco());
	}
}
