package com.designpatterns.gof.structural.decorator;

/**
 * Concrete Decorator
 * @author 05155211328
 *
 */
public class Refrigerante extends CoquetelDecorator {
	
	public Refrigerante(Coquetel coquetel) {
		super(coquetel);
		nome = "Refrigerante";
		preco = 1.0;
	}
}
