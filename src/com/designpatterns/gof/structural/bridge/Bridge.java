package com.designpatterns.gof.structural.bridge;

/**
 * Desacopla uma interface de sua implementação, de forma que ambas
 * possam variar independentemente.
 * @author 05155211328
 *
 */
public class Bridge {
	public static void main(String[] args) {
	    JanelaAbstrata janela1 = new JanelaDialogo(new JanelaLinux());
	    janela1.desenhar();
	    System.out.println();
	    
	    JanelaAbstrata janela2 = new JanelaAviso(new JanelaLinux());
	    janela2.desenhar();
	    System.out.println();
	    	
	    JanelaAbstrata janela3 = new JanelaDialogo(new JanelaWindows());
	    janela3.desenhar();
	}
}
