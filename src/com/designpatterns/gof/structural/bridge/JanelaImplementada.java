package com.designpatterns.gof.structural.bridge;

public interface JanelaImplementada {
	 
    void desenharJanela(String titulo);
 
    void desenharBotao(String titulo);
}
