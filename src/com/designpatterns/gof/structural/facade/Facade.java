package com.designpatterns.gof.structural.facade;

/**
 * Oferece uma interface unificada para um conjunto de interfaces em um
 * subsistema, definindo uma interface de alto nível que facilita a 
 * utilização do subsistema
 * @author 05155211328
 *
 */
public class Facade {
	public static void main(String[] args) {
		System.out.println("##### Configurando subsistemas #####");
		SistemasFacade fachada = new SistemasFacade();
		fachada.inicializarSubsistemas();
		System.out.println();
		System.out.println("##### Utilizando subsistemas #####");
		fachada.renderizarImagem("imagem.png");
		fachada.reproduzirAudio("teste.mp3");
		fachada.lerInput();
	}
}
