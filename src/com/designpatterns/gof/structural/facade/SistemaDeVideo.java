package com.designpatterns.gof.structural.facade;

public class SistemaDeVideo {
  
	public void configurarCores() {
        System.out.println("Cores configuradas");
    }
 
    public void configurarResolucao() {
        System.out.println("Resolução configurada");
    }
    
    public void renderizarImagem(String imagem) {
    	System.out.println("Imagem renderizada");
    }
}
