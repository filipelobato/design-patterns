package com.designpatterns.gof.structural.flyweight;

public abstract class SpriteFlyweight {
    public abstract void desenharImagem(Ponto ponto);
}
