package com.designpatterns.gof.structural.adapter;

/** 
 * Converte a interface de uma classe em outra interface que normalmente
 * não poderiam trabalhar juntas por serem incompatíveis.
 * 
 * @author 05155211328
 *
 */
public class Adapter {
    public static void main( String[] args ){
        PatoMarreco pato = new PatoMarreco();
        PeruAustraliano peru = new PeruAustraliano();

        PeruAdapter peruAdapter = new PeruAdapter( peru );
        Pato[] patos = {pato, peruAdapter};

        for( Pato p : patos ){
            p.grasnar();
            System.out.println();
            p.voar();
            System.out.println();
        }
    }
}