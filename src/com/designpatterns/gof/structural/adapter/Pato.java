package com.designpatterns.gof.structural.adapter;

public interface Pato {   
	void grasnar();
    void voar();
}