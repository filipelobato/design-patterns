package com.designpatterns.gof.structural.adapter;

public class PeruAdapter implements Pato {
	/** 
	 * Classe que será adaptada aos métodos da interface do Pato
	 */
    private Peru peru;

    public PeruAdapter( Peru peru ){
        this.peru = peru;
    }

    @Override
    public void grasnar() {
        peru.soar(); /* MÉTODO DE PERU SENDO UTILIZADO DE FORMA ADAPTADA */
    }

    @Override
    public void voar() {
        peru.voar(); /* IGUALMENTE AQUI */
        System.out.println(", voar, voar, voar, voar");
    }
    
}