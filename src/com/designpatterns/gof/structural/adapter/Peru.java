package com.designpatterns.gof.structural.adapter;

public interface Peru {
    public void soar();
    public void voar();
}