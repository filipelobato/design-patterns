package com.designpatterns.gof.structural.proxy;

/** 
 * Proxy � um padr�o de design estrutural que permite fornecer um substituto ou espa�o 
 * reservado para outro objeto. Um proxy controla o acesso ao objeto original, 
 * permitindo que voc� execute algo antes ou depois que a solicita��o chega ao objeto original.
 * @author FILIPE
 *
 */
public class Proxy {
	public static void main(String[] args) {
		System.out.println("Hacker acessando");
		BancoUsuarios banco = new BancoProxy("Hacker", "1234");
		System.out.println(banco.getNumeroDeUsuarios());
		System.out.println(banco.getUsuariosConectados());

		System.out.println("\nAdministrador acessando");
		banco = new BancoProxy("admin", "admin");
		System.out.println(banco.getNumeroDeUsuarios());
		System.out.println(banco.getUsuariosConectados());
		
		System.out.println("\nAcessando direto");
		banco = new BancoUsuarios();
		System.out.println(banco.getNumeroDeUsuarios());
		System.out.println(banco.getUsuariosConectados());
	}
}
