package com.designpatterns.gof.behavioral.chainofresponsability;

/**
 * Evita o acoplamento do remetente de uma requisição ao seu receptor ao dar a mais
 * de um objeto a chance de lidar com a requisição.
 * @author 05155211328
 */
public class ChainOfResponsability {
	public static void main(String[] args) {
	    BancoChain bancos = new BancoA();
	    bancos.setNext(new BancoB());
	    bancos.setNext(new BancoC());
	     
	    try {
	        bancos.efetuarPagamento(IDBancos.bancoC);	        
	        bancos.efetuarPagamento(IDBancos.bancoA);
	        bancos.efetuarPagamento(IDBancos.bancoB);
	        bancos.efetuarPagamento(IDBancos.bancoD);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}