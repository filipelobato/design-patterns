package com.designpatterns.gof.behavioral.chainofresponsability;

public enum IDBancos {
	bancoA, bancoB, bancoC, bancoD
}
