package com.designpatterns.gof.behavioral.interpreter;

import java.util.ArrayList;

public class Interpreter {
	public static void main(String[] args) {
		ArrayList interpretadores = new ArrayList();
	    interpretadores.add(new DoisDigitosRomano());
	    interpretadores.add(new UmDigitoRomano());
	 
	    String numeroRomano = "CXCIV";
	    Contexto contexto = new Contexto(numeroRomano);
	 
	    for (NumeroRomanoInterpreter numeroRomanoInterpreter : interpretadores) {
	        numeroRomanoInterpreter.interpretar(contexto);
	    }
	 
	    System.out.println(numeroRomano + " = "
	            + Integer.toString(contexto.getOutput()));
	}
}
