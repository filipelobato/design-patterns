package com.designpatterns.gof.behavioral.iterator;

public class Iterador {
	public static void main(String[] args) {
		AgregadoCanais canaisDeEsportes = new CanaisEsportes();
		System.out.println("Canais de Esporte:");
		for (IteradorInterface it = canaisDeEsportes.criarIterator(); !it
				.isDone(); it.next()) {
			System.out.println(it.currentItem().nome);
		}
		
		

		AgregadoCanais canaisDeFilmes = new CanaisFilmes();
		System.out.println("\nCanais de Filmes:");
		for (IteradorInterface it = canaisDeFilmes.criarIterator(); !it
				.isDone(); it.next()) {
			System.out.println(it.currentItem().nome);
		}
	}
}
