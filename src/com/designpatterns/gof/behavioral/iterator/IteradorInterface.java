package com.designpatterns.gof.behavioral.iterator;

public interface IteradorInterface {
	void first();

	void next();

	boolean isDone();

	Canal currentItem();
}