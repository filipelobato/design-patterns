package com.designpatterns.gof.behavioral.iterator;

public interface AgregadoCanais {
	IteradorInterface criarIterator();
}