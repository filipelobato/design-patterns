package com.designpatterns.gof.behavioral.strategy;

import java.util.Scanner;

public class Strategy {
	public static void main(String[] args) {
		try(Scanner entrada = new Scanner(System.in)) {
			System.out.print("Informe a distancia: ");
			int distancia = entrada.nextInt();
			System.out.print("Informe o tipo de frete (1) normal (2) sedex: ");
			int opcaoFrete = entrada.nextInt();
			TipoFrete tipoFrete = TipoFrete.values()[opcaoFrete - 1];
			
			Frete frete = new Frete(tipoFrete);
			double preco = frete.calcularPreco(distancia);
			System.out.printf("O valor total é de R$%.2f", preco);
		}
	}
}
		