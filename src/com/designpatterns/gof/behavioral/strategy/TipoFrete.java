package com.designpatterns.gof.behavioral.strategy;

public enum TipoFrete {
	NORMAL,
	SEDEX
}
