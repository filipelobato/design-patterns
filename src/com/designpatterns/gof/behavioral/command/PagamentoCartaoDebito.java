package com.designpatterns.gof.behavioral.command;

public class PagamentoCartaoDebito implements PagamentoCommand {

	@Override
	public void processarCompra(Compra compra) {
		System.out.println("Pagamento via Cartão de Débito criado!\n" + compra.getInfoNota());
	}

}
