package com.designpatterns.gof.behavioral.command;

/**
 * Encapsula a requisição de um objeto, portanto permitindo que se
 * parametrize os clientes com diferentes requisições
 * Exemplo: passa objeto e instância da interface a qual vai se usar método
 */
public class Command {
	public static void main(String[] args) {
	    Loja lojasAfricanas = new Loja("Africanas");
	    lojasAfricanas.executarCompra(999.00, new PagamentoCartaoCredito());
	    System.out.println();
	    lojasAfricanas.executarCompra(49.00, new PagamentoBoleto());
	    System.out.println();
	    lojasAfricanas.executarCompra(99.00, new PagamentoCartaoDebito());
	 
	    Loja exorbitante = new Loja("Exorbitante");
	    exorbitante.executarCompra(19.00, new PagamentoCartaoCredito());
	 
	}
}
