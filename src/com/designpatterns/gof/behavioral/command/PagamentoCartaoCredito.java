package com.designpatterns.gof.behavioral.command;

public class PagamentoCartaoCredito implements PagamentoCommand {

	@Override
	public void processarCompra(Compra compra) {
		System.out.println("Pagamento via Cartão de Crédito criado!\n" + compra.getInfoNota());
	}

}
