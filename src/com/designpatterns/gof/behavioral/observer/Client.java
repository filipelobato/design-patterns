package com.designpatterns.gof.behavioral.observer;

/**
 * Define uma dependência um-para-muitos entre objetos para que,
 * quando um objeto mudar de estado, os seus dependentes sejam notificados e
 * atualizados automaticamente. 
 */
public class Client {
	public static void main(String[] args) {
		DadosSubject dados = new DadosSubject();
		dados.attach(new TabelaObserver(dados));
		dados.attach(new BarraObserver(dados));
		dados.attach(new PorcentoObserver(dados));

		dados.setState(new Dados(9, 4, 5));
		dados.setState(new Dados(2, 3, 1));
	}
}