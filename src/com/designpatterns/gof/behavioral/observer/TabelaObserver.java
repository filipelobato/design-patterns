package com.designpatterns.gof.behavioral.observer;

/**
 * O Observer é o que faz utilização dos dados compartilhados e deve ser atualizado a cada modificação.
 * @author 05155211328
 *
 */
public class TabelaObserver extends DadosObserver {

	public TabelaObserver(DadosSubject dados) {
		super(dados);
	}

	@Override
	public void update() {
		System.out.println("########### TABELA ############");
		System.out.println("\tValor A: " + dados.getState().valorA + "\n\tValor B: " + dados.getState().valorB
				+ "\n\tValor C: " + dados.getState().valorC);
		System.out.println("###############################");
	}

}
