package com.designpatterns.gof.behavioral.observer;
/**
 * O Observer é o que faz utilização dos dados compartilhados e deve ser atualizado a cada modificação.
 * @author 05155211328
 *
 */
public abstract class DadosObserver {

	protected DadosSubject dados;

	public DadosObserver(DadosSubject dados) {
		this.dados = dados;
	}

	public abstract void update();
}