package com.designpatterns.gof.behavioral.mediator;

public interface Mediator {
	
	void enviar(String mensagem, Colleague colleague);
	
}
