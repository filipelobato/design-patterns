package com.designpatterns.gof.behavioral.memento;

import java.util.ArrayList;
import java.util.List;

import com.designpatterns.gof.behavioral.memento.TextoMemento;

public class TextoCareTaker {
	protected List<TextoMemento> estados;
	
	public TextoCareTaker() {
		estados = new ArrayList<TextoMemento>();
	}
	
    public void adicionarMemento(TextoMemento memento) {
        estados.add(memento);
    }
 
    public TextoMemento getUltimoEstadoSalvo() {
        if (estados.size() <= 0) {
            return new TextoMemento("");
        }
        TextoMemento estadoSalvo = estados.get(estados.size() - 1);
        estados.remove(estados.size() - 1);
        return estadoSalvo;
    }
}