package com.designpatterns.gof.behavioral.state;

public interface MarioState {
	MarioState pegarCogumelo();
	
	MarioState pegarFlor();
	
	MarioState pegarPena();
	
	MarioState levarDano();
	
}
